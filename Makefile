YFLAGS = -d

CFLAGS = -Wall

PROGRAM = Parser

OBJS = parser.tab.o lex.yy.o

SRCS = parser.tab.c lex.yy.c 

OUTPUT = output

HEADER = parser.tab.h

CC = gcc

all: $(PROGRAM)

Parser  : parser.tab.o lex.yy.o
	$(CC) $(CFLAGS) -o Parser parser.tab.o lex.yy.o

parser.tab.o :  parser.tab.c
	$(CC) $(CFLAGS) -c parser.tab.c

lex.yy.o : lex.yy.c
	$(CC) $(CLFAGS) -c lex.yy.c

parser.tab.c :  parser.y
	bison $(YFLAGS) parser.y

lex.yy.c : scanner.l
	flex scanner.l

clean : 
	rm -f $(PROGRAM) $(OBJS) $(SRCS) $(HEADER) $(OUTPUT)
