%{
	#include<stdio.h>
	#include<stdlib.h>
	#include<unistd.h>

	#include "parser.tab.h" 
	
	#define YY_DECL int yylex()

	int lines=1;

	void yyerror(char* msg);
	
%}

%option yylineno

digit 		[0-9]
nonzerodigit	[1-9]
letters		[a-zA-Z]


int		-?({nonzerodigit}+{digit}*)
fl1		-?{digit}*\.?{digit}+([eE][-+]?{digit}+)?
fl2		-?({digit}*)?\.{digit}*([eE][-+]?{digit}+)?$
float		{fl1}|{fl2}

str		\"(\\.|[^\\"])*\"
str1		\'(\\.|[^\\'])*\'
string 		{str}|{str1} 

%%

"False"			{return FALSE;}
"True"			{return TRUE;}
"none"			{return NONE;}
"and"			{return AND;}
"or"			{return OR;}
"as"			{return AS;}
"break"			{return BREAK;}
"class"			{return CLASS;}
"continue"		{return CONTINUE;}
"def"			{return DEF;}
"elif"			{return ELIF;}
"else"			{return ELSE;}
"for"			{return FOR;}
"from"			{return FROM;}
"global"		{return GLOBAL;}
"if"			{return IF;}
"import"		{return IMPORT;}
"in"			{return IN;}
"is"			{return IS;}
"lambda"		{return LAMBDA;}
"not"			{return NOT;}
"pass"			{return PASS;}
"return"		{return RETURN;}
"yield"			{return YIELD;}
"__init__"		{return INIT;}
"dict"			{return DICT;}
"items"			{return ITEMS;}
"setdefault"		{return SETDEFAULT;}


", "			{ return ','; }
";"			{ return ';'; }
"]"			{ return ']'; }
"["			{ return '['; }
"("			{ return '('; }
")"			{ return ')'; }
"{"			{ return '{'; }
"}"			{ return '}'; }
":"			{ return ':'; }
"."			{ return '.'; }
"="			{ return ASSIGN; }
"^="			{ return XOR_ASSIGN; }
">>="			{ return RS_ASSIGN; }
"<<="			{ return LS_ASSIGN; }
"**="			{ return POWER_ASSIGN; }

{int}			 {yylval.intval = atoi(yytext); return INT;}
{float}			 {yylval.f = atof(yytext); return FLOAT;}
{string}		 {yylval.str = yytext; return STRING;}
[A-Za-z_]+[0-9A-Za-z_]*  {yylval.str = yytext; return ID;}

"+"			{ return ADD; }
"-"			{ return SUB; }
"*"			{ return WSTAR; }
"**"			{ return POWER; }
"/"			{ return DIV; }
"//"			{ return F_DIV; }
"%"			{ return MOD; }
"^"			{ return XOR; }
"~"			{ return TILDA; }
"<"			{ return LESS; }
">"			{ return BIGGER; }
"<="			{ return LESS_EQUAL; }
">="			{ return BIGGER_EQUAL; }
"=="			{ return EQUAL; }
"!="			{ return NOT_EQUAL; }
"_"			{return '_';}

[ \t\v\f]		{}
"#"[^\n]*		{ } // comments
[\n]			{lines++;}
%%

void yyerror(char* msg)
{
	fprintf(stderr,"[+]Error: %s in Line: %d\n",msg,yylineno);
	
}

