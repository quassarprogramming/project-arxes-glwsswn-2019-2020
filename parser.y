/*
Sunolikos ariu8mos apo 11 s/r kai 18 r/r
*/
%{
	
	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	//#define YYDEBUG 1

	#define MAX_STACK_SIZE 256
	#define TRUE_ 1
	#define FALSE_ 0

	extern char *yytext;
	extern int yylex();
	extern int yylineno;

	FILE *yyin;
	FILE *yyout;
	
	int errors;
	int yywrap();
	
	struct Stack* createStack(unsigned capacity);
	void push(struct Stack* stack,int item);
	void pop(struct Stack* stack);
	int indent_parse(FILE* fp);
	extern void yyerror(char* s);


%}
%locations

%union data{
	int intval;
	char* str,oper;
	float f;
}
/*Declare tokens*/
%token AS BREAK CONTINUE IS LAMBDA NOT RAISE RETURN YIELD TILDA SUB DICT ITEMS SETDEFAULT
%type<intval> INT operators_str operators_num
%type<f> FLOAT print_num numerical_tok 
%type<str> ID STRING


/*Left */
%left ADD AND BIGGER BIGGER_EQUAL DIV XOR_ASSIGN XOR
%left EQUAL F_DIV LS_ASSIGN LESS LESS_EQUAL RS_ASSIGN
%left MOD WSTAR NOT_EQUAL OR POWER POWER_ASSIGN
%left PASS ID INT CLASS FOR FROM FLOAT FALSE ELIF ELSE DEF GLOBAL IF IMPORT IN INIT STRING TRUE ASSIGN NONE

%%
PROGRAM		: PROGRAM declarations
		| 
		;

declarations 	: main_body_decl 
		| class_stmt
		| func_create             
		;

main_body_decl	: import_modules
		| variables
		| for_loop
		| func_declare
		| if_stmt
		| return_content
                | lambda_exp
		| dict_funcs 
		;

/*---------------------------- IMPORT -------------------------------*/
import_modules	: IMPORT module_list 
		| IMPORT module_list AS ID
		| FROM module_list IMPORT module_list 
		| FROM module_list IMPORT module_list AS ID
		| FROM module_list IMPORT WSTAR 
                | FROM '.' IMPORT module_list
		;

module_list 	: ID '.' module_list
                | '.' ID
                | '.''.' ID | '.''.' ID module_list
                | ID        
                | ID',' module_list
                ;


/*---------------------------- VARIABLES -------------------------------*/
variables	: variables_in
		| GLOBAL ID
		| var_sublist ASSIGN tokens_list //  p.x dexetai to x, y, z = (1, func, 3)
		| var_sublist ASSIGN tokens_sub_list // p.x dexetai to x, y, z = 1 , func, 3
		| variable_list ASSIGN tokens_sub_list // p.x dexetai to (x, y, z) = 1 , func, 3
		| var_assign_operators
		;

variables_in	: variable_list ASSIGN tokens_list //p.x dexetai to (x, y, z) = (1, func, 3)
		| assign_list  // p.x mono x = INT,F,S | x=y=z=...= INT|STR|F|func|name
		;

/*---------------------------- DICT -------------------------------*/
dict		: DICT'(''{'dict_list'}'')'
		| '{' dict_list'}'
		;

dict_funcs	: ID'.'SETDEFAULT'('tokens',' tokens')' 
		| ID'.'ITEMS'('')'
		;

dict_list	: tokens_for_assign ':' tokens_for_assign',' dict_list
		| tokens_for_assign ':' tokens_for_assign
		;

/* Kanones ari8mitikon parastasewn   px x=x+t*z,...   */
main_equation	: '('main_equation')' sub_equations // 3 s/r logw main_equation
		| equations 
		;


equations	: '('name operators_num equations')' sub_equations 
		| '('str_tok operators_str equations')' sub_equations
		| '('numerical_tok operators_num equations')' sub_equations
		| name operators_num equations
		| str_tok operators_str string_expression 
		| numerical_tok operators_num equations 
		| name 
		| tokens_for_assign | print_num
                ; 

		

print_num       : numerical_tok operators_num numerical_tok { // 1sh + 17r/r 
						if($2==10){$$ = $1 + $3;printf("%f + %f = %f\n",$1,$3,$$);}
						else if($2==11){$$ = $1 * $3;printf("%f * %f = %f\n",$1,$3,$$);}
						else if($2==12){$$ = $1 - $3;printf("%f - %f = %f\n",$1,$3,$$);}
                                                else if($2==15){$$ = $1 / $3;printf("%f / %f = %f\n",$1,$3,$$);}
						}
    						;

sub_equations	: operators_num equations
		| 
		;


var_assign_operators : name assign_operators main_equation
		     ;

variable_list	: '('name',' var_sublist')'
		;


assign_list	: name ASSIGN sub_assign_list 
                
		;

sub_assign_list : name ASSIGN
		| main_equation
		| dict
		| dict_funcs
		;

var_sublist	: name',' var_sublist
		| name
		;
tokens_list	: '('tokens_for_assign',' tokens_sub_list')'
		;

tokens_sub_list	: tokens_for_assign',' tokens_sub_list
		| tokens_for_assign
		;

// periexei tous tupous dedomenwn, ana8esh sunarthsewn kai lambda sunarthsewn pou xrisimopoiountai gia ana8esh metablhths px. x = f(1) | x = 3.33 ....
tokens_for_assign : tokens
		| func_declare | lambda_exp
		;

tokens		: numerical_tok
		| str_tok
		| NONE 
		;

numerical_tok	: INT {$$ = $1;}
		| FLOAT {$$ = $1;}
		;

str_tok		: STRING
		;

name 		: ID
		| '_'
		| ID'.'ID
		;

operators_num	: operators_str {$$=$1;}
		| SUB {$$=12;}
		| POWER {$$=13;}
		| MOD {$$ = 14;}
		| F_DIV {$$=16;}
		| DIV {$$=15;}
		;

operators_str	: ADD {$$ = 10;}
		| WSTAR {$$ = 11;}
		;


assign_operators : POWER_ASSIGN
		| LS_ASSIGN 
		| RS_ASSIGN 
		| XOR_ASSIGN
		;

/*---------------------------- CLASS -------------------------------*/

class_stmt	: CLASS ID':' methods 
		;

methods		: main_body_decl
		| key_words
		| class_funcs
		;

class_funcs	: DEF INIT express ':' methods
     		| func_create
		;


func_create	: DEF ID express ':' methods
		;

express 	: '(' ')' 
		| '('func_list')'
		;

func_declare	: ID express 
		;

return_content	: RETURN variable_list
		| RETURN var_sublist
		| RETURN tokens_for_assign
		;

func_list	: func_sub_list',' func_list
		| func_sub_list
		;

func_sub_list	: ID
		| tokens_for_assign
		| variables_in
		| variable_list
		;

// ta if paragoun 3 s/r logw kenou kanona
if_stmt		: IF condition  ':' sequence if_sub
                | IF condition  ':' sequence if_sub ELSE ':' sequence
		;

if_sub		: ELIF condition ':' sequence 
		| ELSE ':' sequence
                |
		;

condition	: expression_list 
		| bool_expression | string_expression | num_expression | not_expression
		;

expression_list : name | func_declare |'(' expression_list ')'
                | expression_list sub_operators following1_expr
		| expression_list sub_log_oper following1_expr 
              
		;

bool_expression : boolean_oper | '(' bool_expression ')'
		| bool_expression sub_log_oper following1_expr
		| bool_expression sub_operators following2_expr
		;

num_expression	: numerical_tok | '(' num_expression ')'
		| num_expression sub_operators following2_expr
		| num_expression sub_log_oper following1_expr
                ;

string_expression
                : str_tok | '(' string_expression ')'
		| string_expression sub_log_oper following1_expr
                | string_expression EQUAL following1_expr
                | string_expression NOT_EQUAL following1_expr  
                | string_expression BIGGER string_expression
                | string_expression BIGGER_EQUAL string_expression
                | string_expression LESS string_expression
                | string_expression LESS_EQUAL string_expression
		;

not_expression  : NOT boolean_oper | NOT tokens | NOT ID
                | NOT '(' condition ')'
                ;

following1_expr	: boolean_oper | ID
                | tokens 
                | '(' following1_expr ')'  
                | '(' following1_expr sub_log_oper condition ')' 
                | '(' following1_expr EQUAL condition ')' 
                | '(' following1_expr NOT_EQUAL condition ')'
                | '(' boolean_oper comparison_operators1 following_sub_expr ')' 
                | '(' numerical_tok comparison_operators1  following_sub_expr ')'
                | '(' str_tok comparison_operators1 string_expression ')' 
                | '(' str_tok comparison_operators1 expression_list ')' 
                | '(' ID operators_num following_sub_expr ')'
                | '(' ID comparison_operators1 condition ')'
                ;
               

following_sub_expr : bool_expression 
                | num_expression
                | expression_list
                ;

following2_expr	
        	: boolean_oper 
                | numerical_tok |  ID 
                | '(' following2_expr ')' 
                | '('following2_expr sub_operators condition')' 
                | '('str_tok EQUAL following1_expr')'//  string == me otidipote
                | '('str_tok NOT_EQUAL following1_expr ')'
                | '(' str_tok comparison_operators1 string_expression ')'              
                ;

sub_operators 	: comparison_operators // px >
		| operators_num //px +,-
		;

sub_log_oper	: AND | OR
		;

comparison_operators : BIGGER 
		| BIGGER_EQUAL
		| EQUAL
		| NOT_EQUAL
		| LESS
		| LESS_EQUAL
		;

comparison_operators1 : BIGGER 
		| BIGGER_EQUAL
                | LESS
		| LESS_EQUAL
		;

boolean_oper	: TRUE
		| FALSE
		;


iterator	: ID
		| str_tok
		| func_declare
		;

iterators	: variable_list
		| name
		;

/* ------------- For LOOP -----------------------*/
for_loop	: FOR iterators IN iterator':' sequence for_else 
		;

for_else	: ELSE':' sequence 
		| 
		;

sequence	: main_body_decl
		| key_words
		;

key_words	: PASS | BREAK | CONTINUE | YIELD
		;

/* -------------Lambda functions -----------------------*/
lambda_exp      : LAMBDA params ':' suit_expression 
                | '(' lambda_exp ')' '(' lambda_value_assignment ')'
                ;

params          : var_sublist 
		| 
                ;         

suit_expression : var_sublist | tokens | func_declare 
                | suit_expression operators_num terminating_lambda_expression
                ;


terminating_lambda_expression : var_sublist | tokens | func_declare;  

lambda_value_assignment :  tokens 
			|  tokens ',' lambda_value_assignment  
                	;

%%

/* Dhmiourgia stoibas */
struct Stack{
	int* data;
	unsigned capacity;
	int top;
};

struct Stack* createStack(unsigned capacity) 
{ 
    struct Stack* stack = (struct Stack*)malloc(sizeof(struct Stack)); 
    stack->capacity = capacity; 
    stack->top = -1; 
    stack->data = (int*)malloc(stack->capacity * sizeof(int)); 
    return stack; 
}

void push(struct Stack* stack, int item) 
{
    stack->data[++stack->top] = item; 
}
 
void pop(struct Stack* stack) 
{ 
    stack->data[stack->top--]; 
}

int indent_parse(FILE* fp)
{
	char* line=NULL;
	size_t len=0;
	int line_len=0;
	int line_number=1;
	int comments = 0;
	struct Stack* stack = createStack(MAX_STACK_SIZE);

	push(stack,0);
	printf("STACK TOP: %d\n",stack->data[stack->top]);
	if(fp==NULL)
		return -1;
	while((line_len=getline(&line,&len,fp))!=-1){
			int line_indent_lvl = 0;
			for(int i=0;i<line_len;i++)
			{
				if(line[i]==' '){
					line_indent_lvl+=1;
					if(line[i+1]=='\t'){ fprintf(stderr,"Error: Mix between tabs and spaces %d\n",line_number); return -1;}
				}
				else if(line[i]=='\t'){
					line_indent_lvl+=8;
					if(line[i+1]==' '){ fprintf(stderr,"Error: Mix between tabs and spaces %d\n",line_number); return -1;}
				}
				else{ 
					if(line[i]=='#'){
						comments=1;
						break;
					}else{
						comments=0;
						break; 
					}
				}
			}
			if(comments==0){
				
				if(stack->data[stack->top] < line_indent_lvl){
					/*VIRTUAL INDENT TOKEN IS PRODUCING*/
					printf("INDENT TOKEN: %d < %d\n",stack->data[stack->top],line_indent_lvl);
					push(stack,line_indent_lvl);
					
				}
				else if(stack->data[stack->top] > line_indent_lvl){
					while(stack->data[stack->top] > line_indent_lvl){
						/*VIRTUAL DEDENT TOKEN IS PRODUCING*/
						printf("DEDENT TOKEN %d > %d\n",stack->data[stack->top],line_indent_lvl);
						pop(stack);
					}
					if(stack->data[stack->top] != line_indent_lvl){
						fprintf(stderr,"Indentation error on line %d\n",line_number);
						return -1;
					}
				}
			}
		        line_number+=1;
	}
	/*EOF*/
	printf("EOF\n");
	while(stack->data[stack->top]>0){
		/*VIRTUAL DEDENT TOKEN IS PRODUCING*/
		printf("DEDENT TOKEN\n");
		pop(stack);
	}
	printf("END\n");
	return 0;
}

int main(int argc,char **argv)
{
	FILE *fp,*in;
	char ch;
	int indent_flag=FALSE_;
	/*#ifndef YYEBUG
  		yydebug = 1;
	#endif*/
	
	if(argc==2)
	{
		yyin = fopen(argv[1],"r");
		in = fopen(argv[1],"r");
		printf("INDENTATION CHECK!\n");
		int v = indent_parse(in);
		if(v==0){
			indent_flag = TRUE_;
			printf("INDENTATION SUCCESSFUL!\n");
		}
		else{return -1;}
		printf("PARSING CHECK!\n");
		if((yyparse()==0) && indent_flag==TRUE_){
                       
			fclose(in);
			in = fopen(argv[1],"r");
			fp = fopen("output","w+");
			do{
				ch = fgetc(in);
				if(feof(in)) break;
				fputc(ch,fp);
				fflush(fp);
			}while(1);
			fprintf(stdout,"\rParsing Successful!\n");
		}else{
			fprintf(stderr,"\rParsing Unsuccessful!\n");
			return -1;
		}
		
	}
	else
		fprintf(stderr,"Not the required number of command line arguments\n");
        
       
	
	fclose(yyin);
	fclose(in);
	fclose(fp);

	return 0;
}



int yywrap()
{
	return 1;
}

